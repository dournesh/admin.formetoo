const url = "mongodb://localhost:27017"

const resources = [
    'categories',
    'products',
    'orders',
    'users',
    'clients',
    'attributes',
    'attribute-sets',
    'tabs',
    'tab-sets',
    'statuses',
    'roles',
    'photos',
]

module.exports = {
    url,
    resources
}